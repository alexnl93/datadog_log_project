#!/usr/bin/env python

"""
@author: alexnl022
@date: 11/03/2017
@see: Read a w3c acces log continuously and return the hit number of the most viewed section in the la 10 sec and the last 2 minutes
@var fields: w3c file specification
@var software: w3c file specification
@var start_date: w3c file specification
@var start_time: w3c file specification
@var end_date: w3c file specification
@var end_time: w3c file specification
@var date: w3c file specification
@var time: w3c file specification
@var remark: w3c file specification
@var version: w3c file specification
@var data_tab: global var use by the threads
@var data_tab_lock: global var use by the threads, lock for data_tab
@var dico_lock: global var use by the threads, lock for dico_1 and dico_2
@var stop_thread: global var use by the threads, it is an event to stop all the thread quickly
@var dico_1: global var use by the threads
@var dico_2: global var use by the threads
@var current_local_time: global var use by the threads
@var alert_msg: global var use by the threads
@var program_continue: global var use by the threads

"""

import curses
import datetime
import sys
import unittest
from threading import Thread
from threading import RLock
from threading import Event
from optparse import OptionParser


parser = OptionParser("usage: %prog [options]")
parser.add_option("-i", "--fileinput",
                  help="Path to file log, default is test.log",
                  action="store", default='test.log',
                  dest="input")
parser.add_option("-M", "--maximum",
                  help = "Select the threshold of the alert function, default is 120",
                  action="store", default=120,
                  type="int", dest="maximum")
parser.add_option("-m", "--maximum_percentage_bar",
                  help="Fix the limit hit number for the last 10 seconds, default is 12",
                  action="store", default=12,
                  type="int", dest="maximum_percentage")
parser.add_option("-r", "--refresh_time",
                  help="Refresh time for interface and reader thread, default is 0.5",
                  action="store", default=0.5,
                  type="float", dest="refresh_time")
parser.add_option("-s", "--web_site_name",
                  help="Give the true site name, default is http://my.site.com",
                  action="store", default="http://my.site.com",
                  dest="site_name")

(options, args) = parser.parse_args()

"""
Specification at https://www.w3.org/TR/WD-logfile.html
"""

fields = ['time', 'cs_method', 'cs_uri']
software = ''
start_date = ''
start_time = ''
end_date = ''
end_time = ''
date = ''
time = ''
remark = ''
version = ''

data_tab = []
data_tab_lock = RLock()  # Rlock() allows only one thread to access variable on the contrary of Lock()
dico_lock = RLock()
stop_thread = Event()
dico_1 = {}
dico_2 = {}
current_local_time = datetime.datetime.now()
alert_msg = 'Alert Status off from the beginning'
program_continue = True


def parse_format(line):
    """
    @param line: Line of specification of the w3c log file
    @type line: String
    @summary: Set all the global var of the doc
    @return: Void function
    """
    tab_line = line.split(':')
    input_name = tab_line[0]
    input_body = tab_line[1][1:]
    global version, fields, software, start_date, start_time,\
        end_date, end_time, date, time, remark
    if input_name == 'Version':
        version = input_body
    elif input_name == 'Fields':
        fields = input_body.split(' ')
        fields[len(fields) - 1] = fields[len(fields) - 1][:-1]
        for i in range(len(fields)):
            fields[i] = fields[i].replace('-', '_')
    elif input_name == 'Software':
        software = input_body
    elif input_name == 'Start-Date':
        tab_date = input_body.split()
        start_date = tab_date[0]
        if len(tab_date) == 2:
            start_time = tab_date[1]
    elif input_name == 'End-Date':
        tab_date = input_body.split()
        end_date = tab_date[0]
        if len(tab_date) == 2:
            end_time = tab_date[1]
    elif input_name == 'Date':
        tab_date = input_body.split()
        date = tab_date[0]
        if len(tab_date) == 2:
            time = tab_date[1]
    elif input_name == 'Remark':
        remark = input_body


def alert_message(dic, val, msg, alert_time, hit_num):
    """
    @param dic: Dictionary of string key and int value: it is the subsection of website and the hit number
    @type dic: Dictionary
    @param val: The threshold value
    @type val: Int
    @param msg: Previous alert message
    @type msg: String
    @param alert_time: Alert time of the last alert, or the last time the alert went off
    @type alert_time: Datetime.datetime
    @param hit_num: The previous number of hit when the alarm was activated
    @type hit_num: Int
    @summary: Set or unset the alarm
    @returns: The alert message, the alert time and the hit number
    """
    count = hit_counter(dic)
    if count > val:
        if msg[0] == 'A':
            alert_time = datetime.datetime.now().time()
            hit_num = count
            msg = '/!\ /!\ /!\ Alert trigged at: ' + str(alert_time) + ', Hit number:' + str(count) + ' /!\ /!\ /!\ '
    else:
        if msg[0] == "/":
            alert_time = datetime.datetime.now().time()
            msg = 'Alert Status off since : ' + str(alert_time) + ', Hit number was:' + str(hit_num)
    return msg, alert_time, hit_num


def hit_counter(dic):
    """
    @param dic: Dictionary of string key and int value: it is the subsection of website and the hit number
    @type dic: Dictionary
    @summary: Sum all the hit number to get the total hit number
    @return: The total hit number of a dictionary
    """
    count = 0
    for i in dic:
        count += dic[i]
    return count


def percentage_bar(dic, maxi_val):
    """
    @param dic: Dictionary of string key and int value: it is the subsection of website and the hit number
    @type dic: Dictionary
    @param maxi_val: A number which correspond to the 100 per cent of the bar
    @type maxi_val: Int
    @summary: Return a 'percentage bar' , the percentage is done between the total hit number of the dictionary and the max value
    @return: A string percentage bar
    """
    size_bar = 35
    count = hit_counter(dic)
    mot = '['
    number = min(size_bar, int(size_bar * (float(count)/maxi_val)))
    for i in range(number):
        mot += '*'
    for i in range(size_bar - number):
        mot += ' '
    mot += ']'
    return mot


class Log:

    """
    @summary: Class to analyse the logfile. Each time there is an http request, a log file is create. It has been design to shape easily the output of this program
    """

    def __init__(self, line, my_fields, my_date):
        """
        @param line: The http formatted string log
        @type line: String
        @param my_fields: Contain all the attribute contain in the string line
        @type my_fields: List of string
        @param my_date: The current date, or the date of the w3c log file
        @summary: log builder
        """
        self.cs_uri = None
        tab = line.split(' ')
        for i in range(len(my_fields)):
            setattr(self, my_fields[i], tab[i])
        if 'time' in self.__dict__:
            for fmt in ('%H:%M', '%H:%M:%S', '%H:%M:%S.%f'):
                try:
                    self.time = datetime.datetime.strptime(self.time,
                                                           fmt).time()
                    break
                except ValueError:
                    pass
        else:
            self.time = datetime.datetime.utcnow().time()
        if 'date' in self.__dict__:
            self.date = datetime.datetime.strptime(self.date,
                                                   '%Y-%m-%d').date()
        elif date != '':
            self.date = datetime.datetime.strptime(my_date, '%Y-%m-%d').date()
        else:
            self.date = datetime.datetime.utcnow().date()
        self.datetime = datetime.datetime.combine(self.date, self.time)

    def get_subsection(self):
        """
        @summary: Function that return the subsection of the element:  for "http://my.site.com/pages/create' it is "http://my.site.com/pages"),
        """
        subsection = options.site_name
        cs_uri_el = self.cs_uri
        cs_uri_el = cs_uri_el[1:]
        cs_uri_el = cs_uri_el.split('/')
        for j in range(len(cs_uri_el) - 1):
            subsection = subsection + '/' + cs_uri_el[j]
        return subsection


class Reader(Thread):
    """
     @summary:Class that get a glance on the output of logfile and build object where there is a line. In fact it is the thread which collect data
    """

    def __init__(self):
        Thread.__init__(self)

    def run(self):
        """
         @summary: Thread that collect data until he is told to stop with the var program continue, or until he crash
         """
        global program_continue, stop_thread

        try:
            fp = open(options.input, 'r')

            # Parse the spec of logfile
            line = fp.readline()
            while line[0] == '#':
                parse_format(line[1:])
                line = fp.readline()

            # Get the input
            while program_continue:
                if line:
                    with data_tab_lock:
                        data_tab.append(Log(line, fields, date))
                else:
                    stop_thread.wait(options.refresh_time)
                line = fp.readline()
        except:
            program_continue = False
            stop_thread.set()
            exc_info = sys.exc_info()
            print("Unexpected error in Reader:", exc_info)


class OutputValue(Thread):
    """
    @summary: Class to process data, and delete passed value
    """

    def __init__(self):
        Thread.__init__(self)

    def run(self):
        refresh_time_int = 10
        average_time_check = datetime.timedelta(seconds=120)
        current_time = datetime.datetime.utcnow()
        alert_time = datetime.datetime.now().time()
        alert_hit_number = 0
        global dico_1, dico_2, current_local_time, program_continue, alert_msg, stop_thread

        current_time = current_time - datetime.timedelta(seconds=10)
        try:
            while program_continue:
                with data_tab_lock:
                    # time after the lock, no way to have a future event!
                    last_refresh_time = current_time
                    current_time = datetime.datetime.utcnow()
                    current_local_time = datetime.datetime.now()
                    result_refresh = {}  # Last 10 seconds values
                    result_average = {}  # Last 2 minutes values
                    list_to_delete = []

                    for i in data_tab:
                        # update element
                        if i.datetime < current_time - average_time_check:
                            list_to_delete.append(i)
                        elif i.datetime > last_refresh_time:
                            subsection = i.get_subsection()
                            if subsection in result_refresh:
                                result_refresh[subsection] += 1
                            else:
                                result_refresh[subsection] = 1
                            if subsection in result_average:
                                result_average[subsection] += 1
                            else:
                                result_average[subsection] = 1
                        else:
                            subsection = i.get_subsection()
                            if subsection in result_average:
                                result_average[subsection] += 1
                            else:
                                result_average[subsection] = 1
                    for i in list_to_delete:
                        data_tab.remove(i)
                # idea of not blocking the data for both thread
                # this program should be impact by the interface at all
                with dico_lock:
                    dico_1 = result_refresh
                    dico_2 = result_average
                    alert_msg, alert_time, alert_hit_number = alert_message(dico_2, options.maximum, alert_msg,
                                                                        alert_time, alert_hit_number)
                stop_thread.wait(refresh_time_int)
        except:
            program_continue = False
            stop_thread.set()
            exc_info = sys.exc_info()
            print("Unexpected error in OutputVal:", exc_info)


class ConsoleInterface(Thread):
    """
    @summary: Class for the simple console interface
    """

    def write(self, msg, x, y, w, h):
        """
        @param msg: The message to write on the windows
        @type msg: String
        @param x: The horizontal position of the first letter to write
        @type x: Int
        @param y: The vertical position of the first letter to write
        @type y: Int
        @param w: The width of the area to write
        @type w: Int
        @param h: the high of the area to write
        @type h: Int
        @summary: Write a message in a window area. It stop writing if the area is too small compare to the area
        @return: Void function
        """
        min_size = min(w * h, len(msg))
        for i in range(min_size):
            self.window.move(y + int(i / w), x + i - w * int(i / w))
            self.window.addch(msg[i])

    def draw_line(self, line_number, width):
        """
        @param line_number: The window line to draw
        @type line_number: Int
        @param width: The width of the windows
        @type width: Int
        @summary: Draw a horizontal line in the window
        @return: Void function
        """
        for i in range(width - 2):
            self.window.move(line_number, i + 1)
            self.window.addch('-')

    def draw_column(self, line_number, column_number, high):
        """
        @param line_number: The first window line to draw the column separator
        @type line_number: Int
        @param column_number: The column number to write the separator
        @type column_number: Int
        @param high: The number column separator to print
        @param high: Int
        @summary: Draw column separator in the window
        @return: Void function
        """
        for i in range(high):
            self.window.move(line_number + i, column_number)
            self.window.addch('|')

    def __init__(self):
        Thread.__init__(self)
        self.window = curses.initscr()

    def run(self):
        global program_continue, stop_thread

        try:
            while program_continue:
                global alert_msg
                # Set up the window an get a minimal size
                (h, w) = self.window.getmaxyx()
                window_width_min = 102
                window_high_min = 30
                num_size = 11

                window_width = max(window_width_min, w)
                window_high = max(window_high_min, h)
                self.window.resize(window_high, window_width)
                self.window.border(0)

                # Draw the border of the program
                self.draw_line(2, window_width)
                self.draw_line(4, window_width)
                self.draw_line(7, window_width)
                self.draw_line(9, window_width)

                self.draw_column(1, int(window_width / 2), 1)
                self.draw_column(5, int(window_width / 2), 2)

                self.draw_column(8, int(window_width / 2), 1)
                self.draw_column(8, int(window_width / 2) - num_size, 1)
                self.draw_column(8, window_width - num_size - 1, 1)

                self.draw_column(10, int(window_width / 2), window_high - 11)
                self.draw_column(10, int(window_width / 2) - num_size, window_high - 11)
                self.draw_column(10, window_width - num_size - 1, window_high - 11)

                # Write the content
                self.write("Alex's program log monitor is in action!", 1, 1,
                      int(window_width / 2 - 1), 1)
                self.write("Last refresh time: " + str(current_local_time), 1 + int(window_width / 2), 1,
                      int(window_width / 2 - 1), 1)

                with dico_lock:
                    self.write(alert_msg, 1, 3, window_width - 2, 1)

                    self.write("Hit number in the last 10 sec: " + str(hit_counter(dico_1)), 1, 5,
                          int(window_width / 2 - 1), 1)
                    self.write("Hit number in the last 2 min: " + str(hit_counter(dico_2)),
                          1 + int(window_width / 2), 5, int(window_width / 2 - 1) - 1, 1)

                    self.write(percentage_bar(dico_1, options.maximum_percentage), 1, 6, int(window_width / 2 - 1), 1)
                    self.write(percentage_bar(dico_2, options.maximum), 1 + int(window_width / 2), 6,
                          int(window_width / 2 - 1) - 1, 1)

                    self.write("Section consulted in last 10 seconds", 1, 8, int(window_width / 2 - 1) - num_size, 1)
                    self.write("Hit number", int(window_width / 2) - num_size + 1, 8, num_size - 1, 1)
                    self.write("Section consulted in last 2 minutes", 1 + int(window_width / 2), 8,
                          int(window_width / 2 - 1) - num_size - 1, 1)
                    self.write("Hit number", window_width - num_size, 8, num_size - 1, 1)

                    ordered_key1 = sorted(dico_1, key=dico_1.__getitem__, reverse=True)
                    ordered_key2 = sorted(dico_2, key=dico_2.__getitem__, reverse=True)

                    for i in range(min(len(ordered_key1), window_high - 11)):
                        self.write(ordered_key1[i], 1, 10 + i, int(window_width / 2 - 1) - num_size, 1)
                        self.write(str(dico_1[ordered_key1[i]]), int(window_width / 2) - num_size + 1, 10 + i,
                              num_size - 1, 1)

                    for i in range(min(len(ordered_key2), window_high - 11)):
                        self.write(ordered_key2[i], 1 + int(window_width / 2), 10 + i,
                              int(window_width / 2 - 1) - num_size - 1, 1)
                        self.write(str(dico_2[ordered_key2[i]]), window_width - num_size, 10 + i, num_size - 1, 1)

                # Refresh the page
                self.window.refresh()
                self.window.clear()
                stop_thread.wait(options.refresh_time)
        except:
            program_continue = False
            stop_thread.set()
            exc_info = sys.exc_info()
            print("Unexpected error in ConsoleInterface:",exc_info)
        finally:
            curses.endwin()


class Test(unittest.TestCase):
    """
    @summary: Class to verify if there is no logical error
    """
    def test_alert(self):
        global alert_msg

        # alert_msg = 'Alert Status off from the beginning'
        alert_time = ''
        alert_hit_number = 0

        # dic of hit number = 428
        dic = {'http://my.site.com/foo/barD/helqzd': 30, 'http://my.site.com/foo/barD/helo': 281, 'http://my.site.com/foo/barD': 101, 'http://my.site.com/foo': 16}

        # Alert supposed to be untrigged, msg, time and hitnumber stay the same
        alert_msg, alert_time, alert_hit_number = alert_message(dic, 500, alert_msg,
                                                                alert_time, alert_hit_number)

        self.assertEqual(alert_msg, 'Alert Status off from the beginning')
        self.assertEqual(alert_hit_number, 0)
        self.assertEqual(alert_time, '')

        # Alert trigged, every parameter change
        alert_time = 'day1'
        alert_hit_number = 0
        alert_msg, alert_time, alert_hit_number = alert_message(dic, 200, alert_msg,
                                                                alert_time, alert_hit_number)
        self.assertNotEqual(alert_time, 'day1')
        self.assertEqual(alert_msg, '/!\ /!\ /!\ Alert trigged at: ' + str(alert_time) + ', Hit number:428 /!\ /!\ /!\ ')
        self.assertEqual(alert_hit_number, 428)

        # Alarm still on, nothing should have change
        buf_time = alert_time
        alert_msg, alert_time, alert_hit_number = alert_message(dic, 200, alert_msg,
                                                                alert_time, alert_hit_number)
        self.assertEqual(buf_time, alert_time)
        self.assertEqual(alert_msg,
                          '/!\ /!\ /!\ Alert trigged at: ' + str(alert_time) + ', Hit number:428 /!\ /!\ /!\ ')
        self.assertEqual(alert_hit_number, 428)

        # Increase dico size nothing should change now size 485
        dic = {'http://my.site.com/foo/barD/helqzd': 30, 'http://my.site.com/foo/barD/helo': 281, 'http://my.site.com/foo/barD': 101, 'http://my.site.com/foo': 20}
        # Alarm still on, nothing should have change
        buf_time = alert_time
        alert_msg, alert_time, alert_hit_number = alert_message(dic, 200, alert_msg,
                                                                alert_time, alert_hit_number)
        self.assertEqual(buf_time, alert_time)
        self.assertEqual(alert_msg,
                          '/!\ /!\ /!\ Alert trigged at: ' + str(alert_time) + ', Hit number:428 /!\ /!\ /!\ ')
        self.assertEqual(alert_hit_number, 428)

        # Alarm still just going down, everything has change
        buf_time = alert_time
        alert_msg, alert_time, alert_hit_number = alert_message(dic, 500, alert_msg,
                                                                alert_time, alert_hit_number)
        self.assertNotEqual(alert_time, buf_time)
        self.assertEqual(alert_msg,
                          'Alert Status off since : ' + str(alert_time) + ', Hit number was:428')
        self.assertEqual(alert_hit_number, 428)

        # Alarm stay down, nothing has change
        buf_time = alert_time
        buf_msg = alert_msg
        alert_msg, alert_time, alert_hit_number = alert_message(dic, 500, alert_msg,
                                                                alert_time, alert_hit_number)
        self.assertEqual(alert_time, buf_time)
        self.assertEqual(alert_msg,buf_msg)
        self.assertEqual(alert_hit_number, 428)

        # Alarm stay down, nothing has changed
        alert_msg, alert_time, alert_hit_number = alert_message(dic, 500, alert_msg,
                                                                alert_time, alert_hit_number)
        self.assertEqual(alert_time, buf_time)
        self.assertEqual(alert_msg,buf_msg)
        self.assertEqual(alert_hit_number, 428)

        # Alert trigged, every parameter change
        alert_msg, alert_time, alert_hit_number = alert_message(dic, 200, alert_msg,
                                                                alert_time, alert_hit_number)
        self.assertNotEqual(alert_time, buf_time)
        self.assertEqual(alert_msg, '/!\ /!\ /!\ Alert trigged at: ' + str(alert_time) + ', Hit number:432 /!\ /!\ /!\ ')
        self.assertEqual(alert_hit_number, 432)


def main():
    """
    @summary: Instructions to execute when http_tracker is called directly
    """
    # Creating thread
    reader_thread = Reader()
    output_thread = OutputValue()
    console_thread = ConsoleInterface()

    # Start thread
    reader_thread.start()
    #Time to read data first
    stop_thread.wait(0.2)
    output_thread.start()
    console_thread.start()

    while program_continue:
        stop_thread.wait(1)

    # Wait for thread
    if(console_thread.is_alive()):
        console_thread.join()

    reader_thread.join()
    if(output_thread.is_alive()):
        output_thread.join()

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        program_continue = False
        stop_thread.set()
