HTTP Tracker
============
Licensed under the GPL v3.0

Copyright Alexandre NAHUM

![Capture d'écran 2017-03-19 21.19.47.png](https://bitbucket.org/repo/EgnXXRa/images/1714904960-Capture%20d'%C3%A9cran%202017-03-19%2021.19.47.png)

Requirements
------------
Only Python2 or Python3.

Goal
----
Build a tracker of http log from a w3c formatted log. This tracker allow you to see what happened in the last ten seconds, and also what happened in the last two minutes. Indeed the array indicates all the subsections of the website which has been visited. By default the take file log is test.log which is that repository, but you can change this (please look at **Instalation and usage** section). All the information about w3c logfile are present [here][1]

Installation and usage
----------------------
- Installation: No particular installation is needed except python2 or python3 
- Usage:
  *  Before start, you should create a file named test.log in the main directory.
  *  This is a console program so you just need to run ``` python http_tracker.py ``` or ``` ./python http_tracker.py ``` in your console. If ``` ./python http_tracker.py ``` doesn't work, use this command: ```chmod +x ./http_tracker.py```
  *  You have many running options, to get them you just have to run ``` ./http_tracker.py -h ```. Here they are:

``` bash 
Usage: http_tracker.py [options]

Options:
  -h, --help            show this help message and exit
  -i INPUT, --fileinput=INPUT
                        Path to file log, default is test.log
  -M MAXIMUM, --maximum=MAXIMUM
                        Select the threshold of the alert function, default is
                        120
  -m MAXIMUM_PERCENTAGE, --maximum_percentage_bar=MAXIMUM_PERCENTAGE
                        Fix the limit hit number for the last 10 seconds,
                        default is 12
  -r REFRESH_TIME, --refresh_time=REFRESH_TIME
                        Refresh time for interface and reader thread, default
                        is 0.5
  -s SITE_NAME, --web_site_name=SITE_NAME
                        Give the true site name, default is http://my.site.com
```
Documentation
-------------
There is a code architecture documentation which is generated automaticaly with [epydoc][2] typing ```epydoc --graph classtree --html http_tracker.py -o doc -v```. All this documentation is available in the doc directory, opening the index.html in your favourite web navigator.

Unittest
-------------
To test the alarm logic, run the test: ```python3 -m unittest http_tracker.Test```

Global test
-------------
- Installation: You need python3 and [faker][3]
- Usage: To run the test basicaly to test __http_tracker__, you just have to run w3c_log_generator, ```python3 w3c_log_generator.py```. __I really advice to run this log generator with the -n option__,```python3 w3c_log_generator.py -n``` . Like in the http_tracker program, you have many otpions:   

``` bash 
Usage: w3c_log_generator.py [options]

Options:
  -h, --help            show this help message and exit
  -o OUTPUT, --outputfile=OUTPUT
                        Path to file log, default is test.log
  -c, --write_to_console
                        Write the output to console
  -n, --n               Erease the file to write it from zero
```

Improvement ideas
-------------
- Write all the alerts in a 'alert log file'
- Build a real graphical interface, with chart
- Sort the logs not only by subsection but by the chosen attribute. With the current software architecture it should not be very hard.

[1]: https://www.w3.org/TR/WD-logfile.html
[2]: http://epydoc.sourceforge.net
[3]: https://github.com/joke2k/faker